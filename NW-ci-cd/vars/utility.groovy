import groovy.json.JsonBuilder  
import groovy.json.JsonSlurper
import groovy.transform.Field 
import java.util.regex.Matcher
import java.util.regex.Pattern

@Field boolean isEnvConfigFilecopied = false
@Field boolean isSvcConfigFilecopied = false
@Field boolean isGlobalConfigFilecopied = false

def cleanWorkspace()
    {
        echo "Cleaning up ${WORKSPACE}"
        // clean up our workspace 
        deleteDir()
        // clean up tmp directory 
        dir("${workspace}@tmp") {
            deleteDir()            
        }
        dir("${workspace}@libs") {
            deleteDir()            
        }
    }

def createJenkinsfile(){
     if (fileExists('Jenkinsfile')){
            println('\tJenkinsfile exists not creating...')
        }
        else{
            println('Creating Jenkinsfile for Node project...')
            writeFile file: 'Jenkinsfile', text: '@Library(\'new-shared-library\') _\ndeployment(environment)'
        }
    }
    
def createDockerfile(){
    if (fileExists('package.json')){
        def dockerfile = libraryResource 'Dockerfile'
        println('Identified as Node prject...')
        if (fileExists('Dockerfile')){
            println('\tDockerfile exists not creating...')
        }
        else{
            writeFile file: 'Dockerfile', text: dockerfile
        }
    }
    else{
        println('Unable to identify build type, not creating dockerfile...')
    }
}

def prepareFiles(){
    if (fileExists('package.json')){
        def jenkinsJobTemplate = libraryResource 'template.xml'
        println('Identified as Node prject...')
        if (fileExists('template.xml')){
            println('\ttemplate.xml exists not creating...')
        }
        else{
            writeFile file: 'template.xml', text: jenkinsJobTemplate
        }
    }
    else{
        println('Unable to identify build type, not creating jenkinsJobTemplate...')
    }
}

def createNodeSonarProperty(){
    if (fileExists('package.json')){
        def sonar = libraryResource 'node.sonar'
        if (fileExists('sonar.properties')){
            println('\tsonar properties file exists not creating...')
        }
        else{
            println('Creating sonar.properties for Node project...')
            writeFile file: 'sonar.properties', text: sonar
        }
    }
    else{
        println('Unable to identify build type, not creating sonar.properties...')
    }
}

def cloneRepo(){
    def sout = new StringBuilder(), serr = new StringBuilder()
    command = "git clone https://${REPO_URL}/${ORG}/${APP_NAME}.git; git checkout main"
    def proc = command.execute()
    proc.consumeProcessOutput(sout, serr)
    proc.waitForOrKill(1000)
}


