def call(def pipelineParams){

pipeline {
    agent any
    parameters {
      choice(
        name: 'ENV', 
        choices: ['uat','qa'], 
        description: 'Choose Environment Name'
        )
    }
    
    environment {
        url = "${pipelineParams.url}"
        branch = "${pipelineParams.branch}"
        ENV = "${pipelineParams.ENV}"   
        servicename = "${pipelineParams.servicename}" 
    }

    stages {
        stage('Clean workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Checkout Code') {        
            steps {
                checkout([
                $class: 'GitSCM',
                branches: [[name:  branch ]],
                userRemoteConfigs: [[ url: url ]]
                ])
            }   
        }

 

        stage('deploy') {
            steps {
                echo "**${ENV}****"
                echo "*${servicename}*"
        script{
                if ( "${ENV}" == "uat" ) {
                    echo "true"
                    env.clusterEnvironment="my-cluster"
                    echo "${env.clusterEnvironment}***"
                    }
                    else if( "${ENV}" == "qa" ){
                    env.clusterEnvironment="my-cluster"
                    echo "${env.clusterEnvironment}***"
                    }
                    else if( "${ENV}" == "prod" ){
                    env.clusterEnvironment="my-cluster"
                    echo "${env.clusterEnvironment}***"
                    }

                else{
                    echo "Invalid Environment can't set Cluster Name"
                }
                sh'''
                aws eks update-kubeconfig --region us-east-1 --name $clusterEnvironment
                '''

                if ( "${servicename}" != "null" ) {
                    sh'''
                    #!/bin/bash -x
                    pwd
                    echo $SHELL
                    cat fluentbit-helm-chart/templates/config.yaml  | yq e '.data."input-kubernetes.conf"'  > fluentbit-helm-chart/templates/input.yaml
                    cat << EOF >> fluentbit-helm-chart/templates/input.yaml
[INPUT]
    Name                 tail
    Tag                   demo
EOF
                    ls
                    tree
                    yq -i  eval '.data."input-kubernetes.conf" = "'"\$(< fluentbit-helm-chart/templates/input.yaml)"'"' fluentbit-helm-chart/templates/config.yaml

                    rm -rf fluentbit-helm-chart/templates/input.yaml

                    


 
                    '''
                }
                else{
                    sh'''
                    echo "no srvice added"
                    '''
                }

            }
                sh'''
                helm upgrade --install --set env.name=${ENV} fluent-bit fluentbit-helm-chart -n ${ENV}
                rm -r /var/lib/jenkins/.kube/config
                '''
            }

            }



        }

  
  }    

    }

