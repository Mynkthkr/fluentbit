# Jenkins Pipeline Shared Libraries
This is a collection of shared libraries to be used with
[Jenkins Pipelines](https://jenkins.io/doc/book/pipeline/)
  - **New service Onboard** : we have 'new_onboard.groovy' which auto onboard new services and create a new job .
  - **Googlechat Notification**: Written in groovy/Jenkins Pipeline DSL. Requires Jenkins
  - **Deployment of services** : This groovy is for deployment of the service .

## Dependencies

This project is dependent on Jenkins Pipeline and
Jenkins >= 2.46.3, though some earlier versions may work, they have not been
tested.

## Installation

These shared libraries may be accessed one of three ways:
1. Add this repo to 'Global Pipeline Libraries' in the Jenkins UI.
1. Include a `libraries` block in declarative pipeline syntax.
1. Include this library in an `@Library` statement in a Pipeline script.

## Library Directory Structure

```Shared Libraries have a specific directory structure that tells Jenkins how to load your code and make it available to pipelines.

Here’s the layout, as defined by the Jenkins documentation:

(root)


+- vars

|   +- fooBar.groovy          # for global 'foo' variable

|   +- foo.txt             # help for 'foo' variable

+- resources               # resource files (external libraries only)

|   +- org

|       +- foo

|           +- bar.json    # static helper data for org.foo.Bar
```

### Global Pipeline Libraries

See [this article](https://jenkins.io/doc/book/pipeline/shared-libraries/#global-shared-libraries)
about adding shared libraries via the Jenkins UI.

![](https://jenkins.io/doc/book/resources/pipeline/add-global-pipeline-libraries.png)


### Within Pipeline Script

The declarative way to include libraries is the following.

```
libraries {
    lib('github.com/cfpb/jenkins-shared-libraries')
}
```

Since this repo is hosted on github.com, it's easy to include it in all
script dynamically by adding it to the `@Library` block within a Pipeline script

```
@Library('github.com/cfpb/jenkins-pipeline-shared-libraries') _
```

## Usage

Currently this repo has a `deployment.groovy` `new_onboard.groovy` var that can be invoked after
importing it. Once the entire shared library has been imported, `deployment.groovy`
is available at the top level. E.g.

```
pipeline {
    agent { label "master" }

    libraries {
        lib('github.com/cfpb/jenkins-shared-libraries')
    }

    stages {
        stage("Echostage") {
            steps {
               echo "foo"
            }
        }
    }
    post {
        always {
            script {
                sendEmail(currentBuild, ['testemail@domain.tld'])
            }
        }
    }
}"""
```


## How New service will be onbard using these groovy.

We have created "new_onboard.groovy" for onboarding new service , in this groovy we are using resource directory to store our Sample template of Jenkinsfile , Dockerfle etc . 

STEPS:

1. We will take Git Url as input and then in groovy 1st stage url will be spilited into parts and stored as Enviroment variable like APP_NAME , ORG , REPO_URL.
```
            env.ORG = "${params.REPO_TO_ADD}".split(':')[1].split('/.')[0]
            env.APP_NAME = "${params.REPO_TO_ADD}".split('/')[1].split('\\.')[0]
            if("${params.REPO_TO_ADD}".split('/')[1].contains('@')){
                env.REPO_URL = "${params.REPO_TO_ADD}".split(':')[2].split('@')[1]
            }
            else{
                env.REPO_URL = "${params.REPO_TO_ADD}".split('@')[1].split(':')[0]
            }
```
2. After this stage we will checkout to that git repo provided by user and our groovy will check wether Jenkinsfile and Dockerfile Exists or Not . If NOT then it will push Our Dockerfile and Jenkinsfile in that repo .

```        stage('Create Jenkinsfile and Dockerfile'){
            node('master') {
            utility.createDockerfile()
            utility.createJenkinsfile()
            utility.prepareFiles()
            }
        }
```
As you can see utility.groovy is used , in utility.groovy All funtions are created which we using in main groovy.

3. Now How job is creating using this groovy ?

we are storing a template of running service in our resource file i.e template.xml and by using that template we are creating a replica of the service by replacing $APP_NAME .

Through JnekinsCli we are Executing command in our grrovy to create a job with that template.xml.

``` 
                        sed -i 's/replace_the_repository/${APP_NAME}/g' template.xml 
                        curl -s -X POST 'http://(IP of Jenkins):8080/createItem?name=${APP_NAME}' -u ${JKS_USERNAME}:${JKS_PASSWORD} --data-binary @template.xml -H "Content-Type:text/xml"
                        sleep 20
                        echo "Follow below URL: http://(IP of Jenkins):8080/job/NW-services/job/${APP_NAME}/"
                        curl -X POST -i -u "${JKS_USERNAME}":"${JKS_PASSWORD}" "http://(IP of Jenkins)/job/${APP_NAME}
```

## Getting involved

Adding libraries to this repo is welcome and encouraged. Should you have
an idea for a useful library or an improvement for an existing one fork
this repo and open a PR.

----

## Credits and references

1. [Jenkins Pipeline](https://jenkins.io/doc/book/pipeline/shared-libraries/)
2. [Jenkins Automation](https://github.com/cfpb/jenkins-automation)

